      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <nav class="footer-nav">
              <ul>
                <li>
                  <a href="https://www.creative-tim.com" target="_blank">Musicode Creator</a>
                </li>
                <li>
                  <a href="http://blog.creative-tim.com/" target="_blank">FAQ</a>
                </li>
                <li>
                  <a href="https://www.creative-tim.com/license" target="_blank">Licenses</a>
                </li>
              </ul>
            </nav>
            <div class="credits ml-auto">
              <span class="copyright">
                ©
                <script>
                  document.write(new Date().getFullYear())
                </script>, asky <i class="fa fa-heart heart"></i> template by <a href="https://www.creative-tim.com">Creative Tim</a>
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
</body>
</html>