<!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="utf-8" />
      <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/preloader/azure.gif">
      <link rel="icon" type="image/jpg" href="./assets/img/logoNU.jpg">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <title><?php if($title!=""){ echo $title; }?></title>
      <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
      <!--     Fonts and icons     -->
      
      <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
      <!-- CSS Files -->
      <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" />
      <link href="<?php echo base_url('assets/css/paper-dashboard.css?v=2.0.0'); ?>" rel="stylesheet" />
      <!-- CSS Just for demo purpose, don't include it in your project -->   
      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet)" />
      <style type="text/css">
      .preloader {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background-color: white;
      }
      .preloader .loading {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);
        font: 14px arial;
      }   
      </style>
    </head>

    