<body class="">
  <div class="preloader">
      <div class="loading">
        <img src="<?php echo base_url('assets/img/preloader/azure.gif');?>" width="200">
      </div>
  </div>
  <div class="wrapper ">
    <div class="sidebar" data-color="abu" data-active-color="danger">      
      <div class="logo">
        <a href="#" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="<?php echo base_url('assets/img/elrahma.jpg');?>">
          </div>
        </a>
        <a href="<?php echo site_url(); ?>" class="simple-text logo-normal">
          Music Creator
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="<?php if($this->uri->segment(1)=="creator"){echo"active";}?>">
            <a href="<?php echo site_url('creator');?>">
              <i class="nc-icon nc-single-02"></i>
              <p>Creator</p>
            </a>
          </li>
          <li class="<?php if($this->uri->segment(1)=="song"){echo"active";}?> ">
            <a href="<?php echo site_url('song');?>">
              <i class="nc-icon nc-bell-55"></i>
              <p>Song</p>
            </a>
          </li>
          <li class="<?php if($this->uri->segment(1)=="artist"){echo"active";}?>">
            <a href="<?php echo site_url('artist');?>">
              <i class="nc-icon nc-vector"></i>
              <p>Artist</p>
            </a>
          </li>
          <li class="<?php if($this->uri->segment(1)=="studio"){echo"active";}?>">
            <a href="<?php echo site_url('studio');?>">
              <i class="nc-icon nc-umbrella-13"></i>
              <p>Studio</p>
            </a>
          </li>
          <li class="<?php if($this->uri->segment(1)=="playlist"){echo"active";}?>">
            <a href="<?php echo site_url('playlist');?>">
              <i class="nc-icon nc-sun-fog-29"></i>
              <p>Playlist</p>
            </a>
          </li>
          <li class="<?php if($this->uri->segment(1)=="player"){echo"active";}?>">
            <a href="<?php echo site_url('player');?>">
              <i class="nc-icon nc-diamond"></i>
              <p>Music Players</p>
            </a>
          <li class="<?php if($this->uri->segment(1)=="location"){echo"active";}?>">
            <a href="<?php echo site_url('location');?>">
              <i class="nc-icon nc-pin-3"></i>
              <p>My Location</p>
            </a>
          </li>
        </ul>
      </div>
    </div>