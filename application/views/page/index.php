    <div class="content">
        <div class="row">
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-globe text-warning"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category">Listen up on</p>
                      <p class="card-title"><a href="https://www.jango.com" target="_blank">jango</a><p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-refresh"></i>music streaming on web
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-money-coins text-success"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category">Listen up on</p>
                      <p class="card-title"><a href="https://www.joox.com" target="_blank">JOOX</a><p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-calendar-o"></i> joox.com
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-vector text-danger"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category">Listen up on</p>
                      <p class="card-title"><a href="https://tidal.com/" target="_blank">tidal</a><p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-clock-o"></i> free trial 30 day
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-favourite-28 text-primary"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category">Radio on</p>
                      <p class="card-title"><a href="http://jogjastreamers.com/" target="_blank">JogjaFM</a><p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-refresh"></i> best jogja radio on web
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">Listening Songs</h5>
                <p class="card-category">you like this song?</p>
                <table>
                  <tr>
                    <th>Artist Name</th>
                    <th>title</th>
                    <th>Listening on</th>
                  </tr>
                  <tr>
                    <td>Second Civil</td>
                    <td>Dan Bila</td>
                    <td><audio controls>
                      <source src="mp3/second_civil-dan_bila.mp3" type="audio/mpeg">
                      </audio>
                    </td>
                  </tr>
                  <tr>
                    <td>Sufian Suhaimi</td>
                    <td>Di Matamu</td>
                    <td><audio controls>
                      <source src="mp3/dimatamu-sufian_suhaimi.mp3" type="audio/mpeg" loop>
                      </audio>
                    </td>
                  </tr>
                  <tr>
                    <td>Alan WAlker</td>
                    <td>Lily</td>
                    <td><audio style="display:1" controls>
                      <source src="mp3/lily-alan_walker.mp3" type="audio/mpeg" loop autoplay>
                      </audio>
                    </td>
                  </tr>
                </table>
              </div>
              <div class="card-body ">
                <!--<canvas id=chartHours width="400" height="100"></canvas>-->
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-history"></i> coding + listen + puyeng
                </div>
              </div>
            </div>
          </div>
        </div>
    <!-- DATATABLES -->
    <!-- <script src="<?php //echo base_url('assets/js/core/jquery.min.js'); ?>"></script> -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- <script src="<?php //echo base_url('assets/js/core/bootstrap.min.js'); ?>"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <!--   Core JS Files   -->
    <script type="text/javascript">
      $(document).ready(function() {
          $('.preloader').fadeOut();
      });
    </script>
    
    
  <script src="<?php echo base_url('assets/js/core/popper.min.js'); ?>"></script>
  
  <script src="<?php echo base_url('assets/js/plugins/perfect-scrollbar.jquery.min.js'); ?>"></script>
  <!--  Google Maps Plugin    -->
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->
  <!-- Chart JS -->
  <script src="<?php echo base_url('assets/js/plugins/chartjs.min.js'); ?>"></script>
  <!--  Notifications Plugin    -->
  <script src="<?php echo base_url('assets/js/plugins/bootstrap-notify.js'); ?>"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url('assets/js/paper-dashboard.min.js?v=2.0.0'); ?>" type="text/javascript"></script>
  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="<?php echo base_url('assets/demo/demo.js'); ?>"></script>
<!-- 
  Javascript method's body can be found in assets/assets-for-demo/js/demo.js
      demo.initChartsPages(); -->        