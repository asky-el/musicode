    <div class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card demo-icons">
            <div class="card-header ">
              <h4 class="card-title"> Artist </h4>
            </div>
            <div class="table-responsive" style="padding:20px">
              <div align="LEFT">
                <button type="button" class="btn btn-primary" onclick="add_artist()">Add Artist</button>
                <button class="btn btn-light" onclick="reload_table()">Refresh</button>
              </div>   
              <table id="artist" class="table table-striped table-bordered" >
                <thead><tr>
                    <th>ID</th>
                    <th>Artist Name</th>
                    <th>Action</th>
                  </tr></thead>
                  <tbody>
                    <!-- disini tampilkan data -->
                  </tbody>                
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">ADD ARTIST</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <form action="#" id="form" class="form-horizontal">
                  <div class="form-group">
                      <input name="id" placeholder="ID" id="id" class="form-control" type="text">
                      <span class="help-block text-danger"></span>
                  </div>
                  <div class="form-group">
                      <input name="title" placeholder="Title" id="title" class="form-control" type="text">
                      <span class="help-block text-danger"></span>
                  </div>                              
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->      

  <!-- DATATABLES -->
  <!-- <script src="<?php //echo base_url('assets/js/core/jquery.min.js'); ?>"></script> -->
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <!-- <script src="<?php //echo base_url('assets/js/core/bootstrap.min.js'); ?>"></script> -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <!--   Core JS Files   -->
 
  
  <script type="text/javascript">
      var save_method; //for save method string
      var table;
      
      $(document).ready(function() {
          $('.preloader').fadeOut();
          //datatables
          table = $('#artist').DataTable({ 
      
              "processing": true, //Feature control the processing indicator.
              "serverSide": true, //Feature control DataTables' server-side processing mode.
              "order": [], //Initial no order.
      
              // Load data for the table's content from an Ajax source
              "ajax": {
                  "url": "<?php echo site_url('Artist/data_list');?>",
                  "type": "POST"
              },
      
              //Set column definition initialisation properties.
              "columnDefs": [
              { 
                  "targets": [ -1 ], //last column
                  "orderable": false, //set not orderable
              },
              ],
      
          });
      
          //set input/textarea/select event when change value, remove class error and remove text help block 
          $("input").change(function(){
              $(this).parent().parent().removeClass('has-error');
              $(this).next().empty();
          });
          $("textarea").change(function(){
              $(this).parent().parent().removeClass('has-error');
              $(this).next().empty();
          });
          $("select").change(function(){
              $(this).parent().parent().removeClass('has-error');
              $(this).next().empty();
          });
      
      });

      function add_artist()
      {
          save_method = 'add';
          $('#form')[0].reset(); // reset form on modals
          $('.form-group').removeClass('has-error'); // clear error class
          $('.help-block').empty(); // clear error string
          $('#myModal').modal('show'); // show bootstrap modal
          $('.modal-title').text('Add Artist'); // Set Title to Bootstrap modal title
      }
      
      function edit_artist(id)
      {
          save_method = 'update';
          $('#form')[0].reset(); // reset form on modals
          $('.form-group').removeClass('has-error'); // clear error class
          $('.help-block').empty(); // clear error string
      
          //Ajax Load data from ajax
          $.ajax({
              url : "<?php echo site_url('Artist/data_edit/')?>/" + id,
              type: "GET",
              dataType: "JSON",
              success: function(data)
              {
      
                  $('[name="id"]').val(data.artistid);
                  $('[name="title"]').val(data.artistname);

                  $('#myModal').modal('show'); // show bootstrap modal when complete loaded
                  $('.modal-title').text('Edit Artist Name'); // Set title to Bootstrap modal title
                  $("#id").prop('disabled', true); //disabled form input by ID
      
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error get data from ajax');
              }
          });
      }
      
      function reload_table()
      {
          table.ajax.reload(null,false); //reload datatable ajax 
      }
      
      function save()
      {
          $('#btnSave').text('saving...'); //change button text
          $('#btnSave').attr('disabled',true); //set button disable 
          var url;
      
          if(save_method == 'add') {
              url = "<?php echo site_url('Artist/data_add'); ?>";
          } else {
              url = "<?php echo site_url('Artist/data_update'); ?>";
          }
      
          // ajax adding data to database
          $.ajax({
              url : url,
              type: "POST",
              data: $('#form').serialize(),
              dataType: "JSON",
              success: function(data)
              {
      
                  if(data.status) //if success close modal and reload ajax table
                  {
                      $('#myModal').modal('hide');
                      reload_table();
                  }
                  else
                  {
                      for (var i = 0; i < data.inputerror.length; i++) 
                      {
                          $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                          $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                      }
                  }
                  $('#btnSave').text('save'); //change button text
                  $('#btnSave').attr('disabled',false); //set button enable 
      
      
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error adding / update data');
                  $('#btnSave').text('save'); //change button text
                  $('#btnSave').attr('disabled',false); //set button enable 
      
              }
          });
      }
      
      function delete_artist(id)
      {
          if(confirm('Are you sure?'))
          {
              // ajax delete data to database
              $.ajax({
                  url : "<?php echo site_url('Artist/data_delete')?>/"+id,
                  type: "POST",
                  dataType: "JSON",
                  success: function(data)
                  {
                      //if success reload ajax table
                      $('#modal_form').modal('hide');
                      reload_table();
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error deleting data');
                  }
              });
      
          }
      }
  </script>
  <script src="<?php echo base_url('assets/js/core/popper.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/plugins/perfect-scrollbar.jquery.min.js'); ?>"></script>
  <!-- Chart JS -->
  <script src="<?php echo base_url('assets/js/plugins/chartjs.min.js'); ?>"></script>
  <!--  Notifications Plugin    -->
  <script src="<?php echo base_url('assets/js/plugins/bootstrap-notify.js'); ?>"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url('assets/js/paper-dashboard.min.js?v=2.0.0'); ?>" type="text/javascript"></script>
  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="<?php echo base_url('assets/demo/demo.js'); ?>"></script>  
