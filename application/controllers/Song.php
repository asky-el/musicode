<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Song extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_Song','song');
        $this->load->library('form_validation');
    }

    public function index()
    {
        //data, title di atur sini. bisa di tambahkan lagi
        $header["title"] = "SONG";
        //ini ke view template
        $this->load->view('template/header', $header); // meload file di 'application/views/template/footer'
        $this->load->view('template/sidebar');
        $this->load->view('template/navbar', $header); 
        //kirim data dari database ke view 
        $this->load->view('page/song'); // meload file di 'application/views/page/song'
        $this->load->view('template/footer');
    }

    public function data_list()
    {
        $list = $this->song->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $song) {
            $no++;
            $row = array();
            $row[] = $song->songid;
            $row[] = $song->songname;
            $row[] = $song->songartist;
            $row[] = $song->songreleased;
            $row[] = $song->songgenre;
            $row[] = $song->songlength;
            $row[] = $song->songtype;
    
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_song('."'".$song->songid."'".')"><i class="fa fa-pencil"></i></a>
                    <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_song('."'".$song->songid."'".')"><i class="fa fa-trash"></i></a>';
            
            $data[] = $row;
        }
    
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->song->count_all(),
                        "recordsFiltered" => $this->song->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }   

    public function data_delete($id)
    {
        $this->song->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }

    public function data_add()
    {
        $this->_validate();
        $data = array(
                'songid' => $this->input->post('id'),
                'songname' => $this->input->post('title'),
                'songartist' => $this->input->post('artist'),
                'songreleased' => $this->input->post('released'),
                'songgenre' => $this->input->post('genre'),
                'songlength' => $this->input->post('length'),
                'songtype' => $this->input->post('type')
            );
        $insert = $this->song->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function data_edit($id)
    {
        $data = $this->song->get_by_id($id);
        echo json_encode($data);
    }
 
    public function data_update()
    {
        $this->_validate();
        $data = array(
                'songid' => $this->input->post('id'),
                'songname' => $this->input->post('title'),
                'songartist' => $this->input->post('artist'),
                'songreleased' => $this->input->post('released'),
                'songgenre' => $this->input->post('genre'),
                'songlength' => $this->input->post('length'),
                'songtype' => $this->input->post('type')
            );
        $this->song->update(array('songid' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('id') == '')
        {
            $data['inputerror'][] = 'id';
            $data['error_string'][] = 'id harus diisi bos';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('title') == '')
        {
            $data['inputerror'][] = 'title';
            $data['error_string'][] = 'title harus diisi bos';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('artist') == '')
        {
            $data['inputerror'][] = 'artist';
            $data['error_string'][] = 'artist harus diisi bos';
            $data['status'] = FALSE;
        }
 
        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
               
}