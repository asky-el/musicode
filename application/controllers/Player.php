<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Player extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_Player','playerl');
        $this->load->library('form_validation');
    }

    public function index()
    {
            $header["title"] = "MUSIC PLAYER";
            $this->load->view('template/header', $header);
            $this->load->view('template/sidebar');
            $this->load->view('template/navbar', $header);
            $this->load->view('page/player');
            $this->load->view('template/footer');
    }   

    public function data_list()
    {
        $list = $this->playerl->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $player) {
            $no++;
            $row = array();
            $row[] = $player->playerid;
            $row[] = $player->playername;
            $row[] = $player->developers;
            $row[] = $player->website;
            $row[] = $player->description;    
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_player('."'".$player->playerid."'".')"><i class="fa fa-pencil"></i></a>
                    <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_player('."'".$player->playerid."'".')"><i class="fa fa-trash"></i></a>';
            
            $data[] = $row;
        }
    
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->playerl->count_all(),
                        "recordsFiltered" => $this->playerl->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }                   
    public function data_delete($id)
    {
        $this->player->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
    public function data_add()
    {
        $this->_validate();
        $data = array(
                'playerid' => $this->input->post('id'),
                'playername' => $this->input->post('playertitle'),
                'developers' => $this->input->post('developers'),
                'website' => $this->input->post('website'),
                'description' => $this->input->post('description'),
            );
        $insert = $this->playerl->save($data);
        echo json_encode(array("status" => TRUE));
    }
    public function data_edit($id)
    {
        $data = $this->playerl->get_by_id($id);
        echo json_encode($data);
    }
    public function data_update()
    {
        $this->_validate();
        $data = array(
                'playerid' => $this->input->post('id'),
                'playername' => $this->input->post('playertitle'),
                'developers' => $this->input->post('developers'),
                'website' => $this->input->post('website'),
                'description' => $this->input->post('description'),
            );
        $this->playerl->update(array('playerid' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('id') == '')
        {
            $data['inputerror'][] = 'id';
            $data['error_string'][] = 'id harus diisi bos';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('playertitle') == '')
        {
            $data['inputerror'][] = 'playertitle';
            $data['error_string'][] = 'title harus diisi bos';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('developers') == '')
        {
            $data['inputerror'][] = 'developers';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('website') == '')
        {
            $data['inputerror'][] = 'website';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('description') == '')
        {
            $data['inputerror'][] = 'description';
            $data['status'] = FALSE;
        }
        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }

}