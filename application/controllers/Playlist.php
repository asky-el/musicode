<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Playlist extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_Playlist','playlists');
        $this->load->library('form_validation');
    }

    public function index()
    {
            $header["title"] = "PLAYLIST";
            $this->load->view('template/header', $header);
            $this->load->view('template/sidebar');
            $this->load->view('template/navbar', $header);
            $this->load->view('page/playlist');
            $this->load->view('template/footer');
    }                      

    public function data_list()
    {
        $list = $this->playlists->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $playlist) {
            $no++;
            $row = array();
            $row[] = $playlist->playid;
            $row[] = $playlist->playtitle;
            $row[] = $playlist->playlength;
            $row[] = $playlist->playgenre;    
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_playlist('."'".$playlist->playid."'".')"><i class="fa fa-pencil"></i></a>
                    <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_playlist('."'".$playlist->playid."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
    }

    $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->playlists->count_all(),
                        "recordsFiltered" => $this->playlists->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
    public function data_delete($id)
    {
        $this->playlists->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }

    public function data_add()
    {
        $this->_validate();
        $data = array(
                'playid' => $this->input->post('id'),
                'playtitle' => $this->input->post('playtitle'),
                'playlength' => $this->input->post('length'),
                'playgenre' => $this->input->post('genre')
            );
        $insert = $this->playlists->save($data);
        echo json_encode(array("status" => TRUE));
    }
    public function data_edit($id)
    {
        $data = $this->playlists->get_by_id($id);
        echo json_encode($data);
    }
 
    public function data_update()
    {
        $this->_validate();
        $data = array(
                'playid' => $this->input->post('id'),
                'playtitle' => $this->input->post('playtitle'),
                'playlength' => $this->input->post('length'),
                'playgenre' => $this->input->post('genre')
            );
        $this->playlists->update(array('playid' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('id') == '')
        {
            $data['inputerror'][] = 'id';
            $data['error_string'][] = 'required ID';
            $data['status'] = FALSE;
        }

        if($this->input->post('playtitle') == '')
        {
            $data['inputerror'][] = 'playtitle';
            $data['error_string'][] = 'required title';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
}