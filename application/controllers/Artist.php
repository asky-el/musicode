<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Artist extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_Artist','artist');
        $this->load->library('form_validation');
    }

    public function index()
    {
            $header["title"] = "ARTIST";
            $this->load->view('template/header', $header);
            $this->load->view('template/sidebar');
            $this->load->view('template/navbar', $header);
            $this->load->view('page/artist');
            $this->load->view('template/footer');
    }


    public function data_list()
    {
        $list = $this->artist->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $art) {
            $no++;
            $row = array();
            $row[] = $art->artistid;
            $row[] = $art->artistname;    
            
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_artist('."'".$art->artistid."'".')"><i class="fa fa-pencil"></i></a>
                    <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_artist('."'".$art->artistid."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
    }
    $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->artist->count_all(),
                        "recordsFiltered" => $this->artist->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
    public function data_delete($id)
    {
        $this->artist->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }

    public function data_add()
    {
        $this->_validate();
        $data = array(
                'artistid' => $this->input->post('id'),
                'artistname' => $this->input->post('title')
            );
        $insert = $this->artist->save($data);
        echo json_encode(array("status" => TRUE));
    }
    public function data_edit($id)
    {
        $data = $this->artist->get_by_id($id);
        echo json_encode($data);
    }
 
    public function data_update()
    {
        $this->_validate();
        $data = array(
                'artistid' => $this->input->post('id'),
                'artistname' => $this->input->post('title')
            );
        $this->artist->update(array('artistid' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('id') == '')
        {
            $data['inputerror'][] = 'id';
            $data['error_string'][] = 'required ID';
            $data['status'] = FALSE;
        }

        {
            $data['inputerror'][] = 'title';
            $data['error_string'][] = 'required title';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }

}