<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Location extends CI_Controller {

        public function index()
        {
                $header["title"] = "LOCATION";
                $this->load->view('template/header', $header);
                $this->load->view('template/sidebar');
                $this->load->view('template/navbar', $header);
                $this->load->view('page/location');
                $this->load->view('template/footer');
        }                      
}