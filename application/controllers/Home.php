<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {

        public function index()
        {
                $header["title"] = "MUSICODE";
                $this->load->view('template/header', $header);
                $this->load->view('template/sidebar');
                $this->load->view('template/navbar', $header);
                $this->load->view('page/index');
                $this->load->view('template/footer');
        }                      
}
