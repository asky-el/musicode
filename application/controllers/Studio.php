<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Studio extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_Studio','studio');
        $this->load->library('form_validation');
    }

    public function index()
    {
            $header["title"] = "STUDIO";
            $this->load->view('template/header', $header);
            $this->load->view('template/sidebar');
            $this->load->view('template/navbar', $header);
            $this->load->view('page/studio');
            $this->load->view('template/footer');
    }                      

    public function data_list()
    {
        $list = $this->studio->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $studio) {
            $no++;
            $row = array();
            $row[] = $studio->studioid;
            $row[] = $studio->studioname;
            $row[] = $studio->address;
            $row[] = $studio->country;
    
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_studio('."'".$studio->studioid."'".')"><i class="fa fa-pencil"></i></a>
                    <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_studio('."'".$studio->studioid."'".')"><i class="fa fa-trash"></i></a>';
            
            $data[] = $row;
        }
    
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->studio->count_all(),
                        "recordsFiltered" => $this->studio->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }   

    public function data_delete($id)
    {
        $this->studio->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }

    public function data_add()
    {
        $this->_validate();
        $data = array(
                'studioid' => $this->input->post('id'),
                'studioname' => $this->input->post('studio'),
                'address' => $this->input->post('address'),
                'country' => $this->input->post('country'),
            );
        $insert = $this->studio->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function data_edit($id)
    {
        $data = $this->studio->get_by_id($id);
        echo json_encode($data);
    }
 
    public function data_update()
    {
        $this->_validate();
        $data = array(
                'studioid' => $this->input->post('id'),
                'studioname' => $this->input->post('studio'),
                'address' => $this->input->post('address'),
                'country' => $this->input->post('country'),
            );
        $this->studio->update(array('studioid' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('id') == '')
        {
            $data['inputerror'][] = 'id';
            $data['error_string'][] = 'id required';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('studio') == '')
        {
            $data['inputerror'][] = 'studio';
            $data['error_string'][] = 'studio name required';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('address') == '')
        {
            $data['inputerror'][] = 'address';
            $data['error_string'][] = 'need address';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('country') == '')
        {
            $data['inputerror'][] = 'country';
            $data['error_string'][] = 'please input the country name';
            $data['status'] = FALSE;
        }
        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
}